import { Routes, Route } from "react-router-dom";
import Home from "./Page/Home";
import "./App.css";
import Navbar from "./Component/Navbar";
import {SidebarMobile, SideBarDesktop} from "./Component/Sidebar";
import {useState} from 'react';
import Tokenomic from "./Page/Tokenomic";
import Features from "./Page/Features";
import Listing from "./Page/Listing";
import Community from "./Page/Community";
import Teams from "./Page/Teams";
import Partnership from "./Page/Partnership";
import Security from "./Page/Security";
import Risk from "./Page/Risk";
import Conclution from "./Page/Conclution";
import {CSSTransition, TransitionGroup } from "react-transition-group";
import { useLocation } from "react-router-dom";



function App() {
  const [navOpen, setNavOpen] = useState(false);
  const [onActive, setOnActive] = useState();
  const location = useLocation();
  
  return (
    <div className="App">
      <div className="layout w-full h-screen bg-[#01062E]">
        <Navbar  navOpen={navOpen} setNavOpen={setNavOpen}/>
        <div className="contentwrap w-full flex items-start">.
         <div className={`sidebars 2xl:w-[300px] xl:w-[300px] lg:w-[300px] w-0 flex bg-[#01062E] `}>
  <SidebarMobile onActive={onActive} setOnActive={setOnActive} navOpen={navOpen} setNavOpen={setNavOpen} />
<SideBarDesktop onActive={onActive} setOnActive={setOnActive} />
          </div>
          <div className="content flex-1 justify-center ">
            <TransitionGroup>
              <CSSTransition key={location.key} classNames="fade" timeout={300}>
            <Routes>
              <Route path="/" element={<Home onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/tokenomic"  element={< Tokenomic onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/features"  element={<Features onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/listing"  element={<Listing onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/community"  element={<Community onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/teams"  element={<Teams onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/partnership"  element={<Partnership onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/security"  element={<Security onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/risk"  element={<Risk onActive={onActive} setOnActive={setOnActive} />} />
              <Route path="/conclution"  element={<Conclution onActive={onActive} setOnActive={setOnActive} />} />
            </Routes>
            </CSSTransition>
            </TransitionGroup>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;