import React from "react";
import Banner1 from "../Asset/Bannersm.png";

import NextBtn from "../Component/Button/NextBtn";
import PrevBtn from "../Component/Button/PrevBtn";

const Tokenomic = ({ setOnActive, onActive }) => {
  return (
    <div className="Tokenomiccontent w-full h-full overflow-y-hidden overflow-x-hidden">
      <div className="wraptokenomic 2xl:w-[90%] w-[90%] mx-auto 2xl:mx-[100px] ">
        <div className="titles w-full flex mt-[30px]">
          <div className="contains gap-1 2xl:mb-12 2xl:mt-4 xl:mb-12 xl:mt-4 lg:mb-12 lg:mt-4 flex flex-col text-left">
            <img src={Banner1} alt="" />
            <p className="text-white  font-bold 2xl:text-[40px] xl:text-[40px] lg:text-[40px] text-[30px] 2xl:mt-[100px]xl:mt-[100px] lg:mt-[100px] mt-[30px] ">
              Coin Symbol and Contract
            </p>
            <p className="text-white font-bold 2xl:text-[26px] xl:text-[26px] lg:text-[26px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Coin Symbol
            </p>
            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              $XYZMER
            </p>
            <p className="text-white font-bold 2xl:text-[26px] xl:text-[26px] lg:text-[26px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Coin Contract Address:
            </p>
            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              0x2bF4BE7C4520C41d012EB09a034179E03b898534
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px]">
              Network:
            </p>
            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              BSC (Binance Smart Chain)
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              XYZMER has a total supply of 666,666,666 Coins.
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[16px]  mb-5">
              According to the allocation plan:
            </p>
            <ul className="list-disc text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] ml-5 ">
              <li>Marketing : 3.4%</li>
              <li>R&D : 20%</li>
              <li>Exchanges 10%</li>
              <li>Circulating Supply 66.6%</li>
            </ul>
            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[50%] xl:w-[50%] lg:w-[50%] w-full 2xl:mt-[70px] xl:mt-[70px] lg:mt-[70px] mt-[60px] mb-[50px]">
              We have chosen Binance Smart Chain as the network for XYZMER COIN,
              providing efficient transaction speeds and low transaction fees.
              It is also connected to the Binance ecosystem and many DeFi
              applications, offering more opportunities and growth potential for
              XYZMER COIN development
            </p>
            <div className="wrap w-full h-full 2xl:flex xl:flex lg:flex 2xl:flex-row xl:flex-row lg:flex-row flex-col justify-center items-center 2xl:gap-10 xl:gap-10 lg:gap-10 ">
              <PrevBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="XYZMER COIN Whitepaper"
                link="/"
              />
              <NextBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Features & Innovations"
                link="/features"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tokenomic;
