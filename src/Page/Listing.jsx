import React from "react";
import Banner1 from "../Asset/Bannersm.png";
import NextBtn from "../Component/Button/NextBtn";
import PrevBtn from "../Component/Button/PrevBtn";

const Listing = ({ setOnActive, onActive }) => {
  return (
    <div className="Listingcontent w-full h-full overflow-y-hidden overflow-x-hidden">
      <div className="wraptitles 2xl:w-[90%] w-[90%] mx-auto 2xl:mx-[100px] ">
        <div className="titles w-full flex mt-[30px]">
          <div className="contains gap-3 2xl:mb-12 2xl:mt-4 xl:mb-12 xl:mt-4 lg:mb-12 lg:mt-4 flex flex-col text-left">
            <img src={Banner1} alt="" />
            <p className="text-white  font-bold 2xl:text-[40px] xl:text-[40px] lg:text-[40px] text-[30px]  mt-[60px] ">
              Listing Plan XYZMER
            </p>
            <p className="text-white  font-bold 2xl:text-[26px] xl:text-[26px] lg:text-[26px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Active Collaboration Commitment with Exchanges: Ensuring a
              Seamless Trading
            </p>

            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              XYZMER COIN aims to actively collaborate with exchanges to ensure
              a seamless trading experience. Once 70% of the Pink Sale presale
              is distributed, our focus will shift to seeking listings on
              leading cryptocurrency exchanges. We aim to get listed on major
              exchanges such as Huobi, OKEX, Gate.io, LBank, Bybit, and others
              to increase project exposure and liquidity.
            </p>
            
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px]xl:mt-[100px] lg:mt-[100px] mt-[60px]">
              Listing on reputable exchanges offers several benefits to the
              XYZMER ecosystem, including:
            </p>
            <ul className="list-disc w-[80%] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] text-white ml-10 mt-5">
              <li>
                Increased Visibility: Being listed on major exchanges exposes
                the project to a broader audience of traders and investors,
                enhancing its visibility within the crypto community.
              </li>
              <li className="mt-5">
                Enhanced Liquidity: By trading on trustworthy exchanges, XYZMER COIN
                users will have access to a larger pool of potential buyers and
                sellers, ensuring improved liquidity and better price discovery
                for the token.
              </li>
              <li className="mt-5">
                Trust and Credibility: Partnering with well-known exchanges adds
                credibility and trustworthiness to the XYZMER COIN project,
                reassuring users of the token's legitimacy and security.
              </li>
            </ul>

            <p className="text-white mt-10 font-bold 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full ">
              We are committed to collaborating with exchange partners, meeting
              their listing requirements, and ensuring a seamless and convenient
              trading experience for XYZMER COIN holders.
            </p>

            <div className="wrap w-full h-full 2xl:flex xl:flex lg:flex 2xl:flex-row xl:flex-row lg:flex-row flex-col justify-center items-center 2xl:gap-10 xl:gap-10 lg:gap-10 ">
              <PrevBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Partnerships and Collaboration"
                link="/partnership"
              />
              <NextBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Community XYZMER COIN"
                link="/community"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Listing;
