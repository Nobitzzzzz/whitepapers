import React from "react";
import Banner1 from "../Asset/Bannersm.png";
import NextBtn from "../Component/Button/NextBtn";
import PrevBtn from "../Component/Button/PrevBtn";

const Partnership = ({ setOnActive, onActive }) => {
  return (
    <div className="Partnershipcontent w-full h-full overflow-y-hidden overflow-x-hidden">
      <div className="wraptitles 2xl:w-[90%] w-[90%] mx-auto 2xl:mx-[100px] ">
        <div className="titles w-full flex mt-[30px]">
          <div className="contains gap-3 2xl:mb-12 2xl:mt-4 xl:mb-12 xl:mt-4 lg:mb-12 lg:mt-4 flex flex-col text-left">
            <img src={Banner1} alt="" />
            <p className="text-white  font-bold 2xl:text-[40px] xl:text-[40px] lg:text-[40px] text-[30px]  mt-[30px] ">
              Partnerships and Collaboration
            </p>
            <p className="text-white  font-bold 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[16px]  mt-[10px] ">
              Forging Success Through Strategic Partnerships
            </p>

            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]  mt-[50px]">
              XYZMER COIN understands that strategic partnerships are crucial
              for achieving long-term success and innovation. Therefore, XYZMER
              COIN is committed to collaborating with various entities within
              the blockchain ecosystem and the creative industry. This includes,
              but is not limited to, partnerships with cryptocurrency payment
              service providers, blockchain technology companies, creative
              content creators, and other organizations that share our vision
              for a transparent and equitable future.
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]  mt-[10px]">
              By joining forces with cryptocurrency payment service providers,
              XYZMER COIN aims to simplify transactions and broaden the use
              cases for our coin, making it more accessible to users worldwide.
              Collaborations with blockchain technology companies will enable us
              to leverage cutting-edge advancements, ensuring our platform
              remains at the forefront of the industry. Furthermore, working
              with creative content creators and industry influencers will help
              us foster a vibrant community, driving user engagement and
              adoption.
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]  mt-[10px]">
              These partnerships are not just about expanding our network; they
              are about creating a synergistic environment where innovation
              thrives, user experience is paramount, and every stakeholder
              benefits. Together, we can drive growth, enhance security, and
              build a more robust and sustainable ecosystem for all involved.
              XYZMER COIN is excited to embark on this journey of collaboration
              and innovation, paving the way for a brighter future in the
              blockchain and creative industries.
            </p>
            

            <div className="wrap w-full h-full 2xl:flex xl:flex lg:flex 2xl:flex-row xl:flex-row lg:flex-row flex-col justify-center items-center 2xl:gap-10 xl:gap-10 lg:gap-10 ">
              <PrevBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Features And Inovation"
                link="/features"
              />
              <NextBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Listing Plan XYZMER COIN"
                link="/listing"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Partnership;
