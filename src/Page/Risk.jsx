import React from "react";
import Banner1 from "../Asset/Bannersm.png";
import NextBtn from "../Component/Button/NextBtn";
import PrevBtn from "../Component/Button/PrevBtn";

const Risk = ({setOnActive, onActive}) => {
  return (
    <div className="riskcontent w-full h-full overflow-y-hidden overflow-x-hidden">
      <div className="wraptitles 2xl:w-[90%] w-[90%] mx-auto 2xl:mx-[100px] ">
        <div className="titles w-full flex mt-[30px]">
          <div className="contains gap-3 2xl:mb-12 2xl:mt-4 xl:mb-12 xl:mt-4 lg:mb-12 lg:mt-4 flex flex-col text-left">
            <img src={Banner1} alt="" />
            <p className="text-white  font-bold 2xl:text-[40px] xl:text-[40px] lg:text-[40px] text-[30px]  mt-[60px] ">
              Risk Disclosure
            </p>
            <p className="text-white  font-bold 2xl:text-[26px] xl:text-[26px] lg:text-[26px] text-[20px] mt-[10px] ">
              Understanding Potential Risks in XYZMER COIN Investment
            </p>
            <p className="text-white  font-bold 2xl:text-[26px] xl:text-[26px] lg:text-[26px] text-[20px] 2xl:mt-[100px]xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Risk Disclaimer
            </p>

            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] ">
              Before engaging in XYZMER COIN investment, it is crucial to consider
              the following risk factors:
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px]xl:mt-[100px] lg:mt-[100px] mt-[60px]">
              Market Risk
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              The cryptocurrency market is highly volatile and subject to
              unpredictable fluctuations. Token prices are influenced by various
              factors such as market demand, competition, regulatory changes,
              and more. Investors are advised to carefully assess these risks
              and base their decisions on their risk tolerance.
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Technology Risk
            </p>

            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full ">
              Blockchain technology involves inherent technological risks,
              including network attacks, smart contract vulnerabilities, and
              security issues. While XYZMER COIN will implement security measures,
              complete risk elimination is not guaranteed.
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Legal and Compliance Risk
            </p>

            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full  ">
              The cryptocurrency industry is subject to regulatory oversight,
              posing compliance risks in different countries. XYZMER COIN will
              collaborate with legal experts to adhere to applicable laws and
              regulations, but changes in the legal environment may impact the
              project.
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Execution Risk
            </p>

            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full  ">
              Despite our best efforts, there are execution risks associated
              with the success of XYZMER COIN. The project's success depends on the
              team's capabilities, market acceptance, and various factors.
              Investors should acknowledge potential challenges and
              uncertainties during the execution process.
            </p>
            <p className="text-white font-bold 2xl:text-[28px] xl:text-[28px] lg:text-[28px] text-[20px] 2xl:mt-[100px] xl:mt-[100px] lg:mt-[100px] mt-[60px] ">
              Crowdfunding Risk
            </p>

            <p className="text-white 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full  ">
            XYZMER COIN issuance and distribution will occur through
              crowdfunding, presenting risks such as fund security and the
              project's ability to meet expected goals. Investors should
              carefully evaluate investment amounts and understand associated
              risks.
            </p>

            <p className="text-white italic 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full 2xl:mt-[50px] xl:mt-[50px] lg:mt-[50px] mt-[30px]  ">
              Note: The above risk factors serve as examples, and actual risk
              factors may be more complex and diverse. Investors are encouraged
              to independently evaluate the risks before participating in the
              XYZMER project and make informed decisions based on their own
              circumstances.
            </p>

            <div className="wrap w-full h-full 2xl:flex xl:flex lg:flex 2xl:flex-row xl:flex-row lg:flex-row flex-col justify-center items-center 2xl:gap-10 xl:gap-10 lg:gap-10 ">
              <PrevBtn onActive={onActive} setOnActive={setOnActive} label="Security and Compliance" link="/security" />
              <NextBtn onActive={onActive} setOnActive={setOnActive} label="Conclution" link="/conclution" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Risk;
