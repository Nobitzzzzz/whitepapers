import React from "react";
import Banner1 from "../Asset/Bannersm.png";
import NextBtn from "../Component/Button/NextBtn";
import PrevBtn from "../Component/Button/PrevBtn";

const Conclution = ({ setOnActive, onActive }) => {
  return (
    <div className="Conclutioncontent w-full h-full overflow-y-hidden overflow-x-hidden">
      <div className="wraptitles 2xl:w-[90%] w-[90%] mx-auto 2xl:mx-[100px] ">
        <div className="titles w-full flex mt-[30px]">
          <div className="contains gap-3 2xl:mb-12 2xl:mt-4 xl:mb-12 xl:mt-4 lg:mb-12 lg:mt-4 flex flex-col text-left">
            <img src={Banner1} alt="" />
            <p className="text-white  font-bold 2xl:text-[40px] xl:text-[40px] lg:text-[40px] text-[30px]  mt-[60px] ">
              Conclusion
            </p>

            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] mt-5">
              XYZMER COIN presents innovative, transparent, and sustainable
              solutions to challenges in the creative industry. By combining
              blockchain technology, Initial Offering, and artificial
              intelligence, XYZMER COIN aspires to lead the future of the
              creative industry towards fairness, dynamism, and competitiveness.
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] mt-10">
              XYZMER COIN is not only fostering synergy among communities but
              also establishing a robust foundation of security and compliance.
              With a focus on diversity and digital connectivity, XYZMER COIN
              aims to lead in shaping an integrated ecosystem that adds value to
              both users and community members.
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] mt-10">
              By merging the music enthusiast, artist fanbase, and crypto
              community, XYZMER COIN aims to create a unique and resilient
              ecosystem. Various elements such as NFTs, the financial support
              approach from the crypto community, and the implementation of
              blockchain for royalties can form a creatively synergistic
              environment.
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] mt-10">
              XYZMER COIN reaffirms its commitment to security through rigorous
              contract audits, the use of multisignature wallets, and continuous
              security monitoring. Furthermore, XYZMER COIN will adhere to local
              laws and regulations, mitigating legal risks and ensuring
              compliance.
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] mt-10">
              Investors need to comprehend the risks associated with the
              volatile cryptocurrency market, blockchain technology risks,
              regulatory compliance challenges, execution risks in project
              implementation, and risks associated with public funding. While
              XYZMER COIN endeavors to manage these risks, investors are
              reminded to conduct independent risk assessments before
              participating.
            </p>

            <div className="wrap w-full h-full 2xl:flex xl:flex lg:flex 2xl:flex-row xl:flex-row lg:flex-row flex-col justify-center items-center 2xl:gap-10 xl:gap-10 lg:gap-10  ">
              <PrevBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Risk Disclosure"
                link="/risk"
              />
              <NextBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Movesix Whitepaper"
                link="/"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Conclution;
