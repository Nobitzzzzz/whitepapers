import React from "react";
import Banner1 from "../Asset/Bannersm.png";
import NextBtn from "../Component/Button/NextBtn";
import PrevBtn from "../Component/Button/PrevBtn";

const Security = ({ setOnActive, onActive }) => {
  return (
    <div className="securitycontent w-full h-full overflow-y-hidden overflow-x-hidden">
      <div className="wraptitles 2xl:w-[90%] w-[90%] mx-auto 2xl:mx-[100px] ">
        <div className="titles w-full flex mt-[30px]">
          <div className="contains gap-3 2xl:mb-12 2xl:mt-4 xl:mb-12 xl:mt-4 lg:mb-12 lg:mt-4 flex flex-col text-left">
            <img src={Banner1} alt="" />
            <p className="text-white  font-bold 2xl:text-[40px] xl:text-[40px] lg:text-[40px] text-[30px]  mt-[30px] ">
              Security and Compliance
            </p>
            <p className="text-white  font-bold 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[16px] mt-[10px] ">
              Safeguarding the XYZMER Ecosystem
            </p>

            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              XYZMER COIN is committed to providing a secure trading environment
              and protecting user funds through strict contract audits,
              multisignature wallets, and ongoing security monitoring.
            </p>

            <p className="text-white mt-10 font-bold 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px] 2xl:w-[70%] xl:w-[70%] lg:w-[70%] w-full ">
              Compliance Requirements
            </p>
            <p className="text-white w-full 2xl:w-[70%] xl:w-[70%] lg:w-[70%] 2xl:text-[20px] xl:text-[20px] lg:text-[20px] text-[12px]">
              XYZMER COIN will comply with local laws and regulations,
              maintaining close cooperation with regulatory authorities. We have
              obtained the necessary licenses to ensure platform compliance and
              reduce the potential for fraudulent activities.
            </p>

            <div className="wrap w-full h-full 2xl:flex xl:flex lg:flex 2xl:flex-row xl:flex-row lg:flex-row flex-col justify-center items-center 2xl:gap-10 xl:gap-10 lg:gap-10 ">
              <PrevBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Team and Advisors"
                link="/teams"
              />
              <NextBtn
                onActive={onActive}
                setOnActive={setOnActive}
                label="Risk Disclosure"
                link="/risk"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Security;
