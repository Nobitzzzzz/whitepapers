import Hands from "../../Asset/hands.png"
import Handshake from "../../Asset/handshake.png"
import Brain from "../../Asset/Brain.png"
// import Notes from "../../Asset/notes.png"
import Shield from "../../Asset/Shield.png"
import Group from "../../Asset/group.png"
import Profile from "../../Asset/Profile.png"
import Conclution from "../../Asset/Cunclution.png" 
// import Tokenomic from "../../Asset/KOIN MOVESIX.png"
import { Notes } from "../iconSVG/IconSVG"
import "../iconSVG/IconSVG.css"



export const sideButton = [
    {
        icon: <Notes className="button stroke-white stroke-2 group-hover:stroke-orange-400"/>,
        title: "Token Economic",
        link: "/tokenomic"
        
    },
    {
        icon: Brain,
        title: "Features and Innovation",
        link: "/features"
        
    },
    {
        icon: Handshake,
        title: "Partnership Relations",
        link: "/partnership"
        
    },
    {
        icon: Hands,
        title: "Listing Plan Movesix",
        link: "/listing"
        
    },
    {
        icon: Group,
        title: "Community Movesix",
        link: "/community"
        
    },
    {
        icon: Profile,
        title: "Team and Advisors",
        link: "/teams"
        
    },
    
    {
        icon: Shield,
        title: "Security And Compliance",
        link: "/security"
        
    },
    {
        icon: Notes,
        title: "Risk Disclosure",
        link: "/risk"
        
    },
    {
        icon: Conclution,
        title: "Conclution",
        link: "/conclution"
        
    },
]
